// Déclaration des broches utilisées
const int ledPins[] = {3, 4, 5, 6, 7, 8}; // Broches pour les 6 LED
const int numLEDs = 6;                   // Nombre total de LED
const int permanentLedPin = 13;          // Broche pour la LED permanente

// Broche pour le bouton
const int buttonPin = 9;

// Variable pour stocker l'état du bouton
int buttonState = 0;
// Variable pour stocker l'état précédent du bouton
int prevButtonState = 0;

void setup() {
  // Initialise les broches des LED en sortie
  for (int i = 0; i < numLEDs; i++) {
    pinMode(ledPins[i], OUTPUT);
  }

  // Initialise la broche de la LED permanente en sortie
  pinMode(permanentLedPin, OUTPUT);
  // Allume la LED permanente au démarrage
  digitalWrite(permanentLedPin, HIGH);

  // Initialise la broche du bouton en entrée
  pinMode(buttonPin, INPUT);
}

void loop() {
  // Lit l'état actuel du bouton (LOW = appuyé, HIGH = relâché)
  buttonState = digitalRead(buttonPin);

  // Vérifie si le bouton a été appuyé (changement d'état de HIGH à LOW)
  if (buttonState == HIGH) {
    // Exécute la séquence
    runSequence();
    delay(5000);
    reverseSequence();
  }

  // Stocke l'état actuel du bouton pour la prochaine itération
  prevButtonState = buttonState;
}

void runSequence() {
  // Allume les LED une à une avec des délais croissants
  for (int i = 0; i < numLEDs; i++) {
    blinkNextLED(i); // Fait clignoter la LED suivante
    turnOnNextLED(i); // Allume la LED suivante de manière fixe
  }
}

void reverseSequence() {
  for (int i = numLEDs; i >= 0; i--) {
      digitalWrite(ledPins[i], LOW);
      delay(200);
    }
}

void blinkNextLED(int index) {
  int ledState = LOW;
  int interval = 750 + (index + 1) * 500;
  unsigned long time = 0;

  while (time <= interval) {
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }
    digitalWrite(ledPins[index], ledState);  // Éteint la LED suivante
    time += 200;
    delay(200);
  }
  
  
}

void turnOnNextLED(int index) {
  digitalWrite(ledPins[index], HIGH); // Allume la LED suivante de manière fixe
}
